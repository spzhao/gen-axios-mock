import MockAdapter from 'axios-mock-adapter'
import {
	get_test_deploy,
	put_test_intra_result,
	get_test_language,
	get_test_work_mode,
	get_interpulse_after_pdw_setting,
	get_config_airspace,
	put_config_airspace,
	post_deploy_setting,
	post_interpulse_knowledge,
	get_knowledge_url,
	post_interpulse_language,
	get_pdw_exist,
	delete_pdw_file,
	delete_file_all,
	get_pdw_files,
	get_pdw_result,
	post_pdw_result,
	get_result_child,
	get_pdw_save,
	post_save_data,
	post_pdw_transfer,
	post_interpulse_pdw_filter,
	get_pdw_config,
	get_pdw_sav,
	get_sav_async,
	put_sav_stop,
	get_radar_mode,
	post_radar_mode,
	get_interpulse_start_read_child_pdw,
	post_interpulse_start_read_child_pdw,
	get_interpulse_start_read_pdw,
	post_interpulse_start_read_pdw,
	post_start_read_pdw_test,
	delete_interpulse_stop_read_child_pdw,
	delete_interpulse_stop_read_pdw,
	post_work_mode,
	get_pdw_files} from './mock-data.js'
const initMock = (axiosInstance) => {
    const mock = new MockAdapter(axioInstance);
    	// 接口：get deploy // get_test_deploy 
	mock.onGet(new RegExp('.*/test/deploy')).reply(200, get_test_deploy);
	// 接口：put intraResult // put_test_intra_result 
	mock.onPut(new RegExp('.*/test/intra_result')).reply(200, put_test_intra_result);
	// 接口：get language // get_test_language 
	mock.onGet(new RegExp('.*/test/language')).reply(200, get_test_language);
	// 接口：get workMode // get_test_work_mode 
	mock.onGet(new RegExp('.*/test/work_mode')).reply(200, get_test_work_mode);
	// 接口：get 获得事后脉间分析PDW的设置 // get_interpulse_after_pdw_setting 
	mock.onGet(new RegExp('.*/v1/interpulse/after_pdw_setting')).reply(200, get_interpulse_after_pdw_setting);
	// 接口：get getAirSpaceConfig // get_config_airspace 
	mock.onGet(new RegExp('.*/v1/interpulse/config/airspace')).reply(200, get_config_airspace);
	// 接口：put setSaveAirSpaceConfig // put_config_airspace 
	mock.onPut(new RegExp('.*/v1/interpulse/config/airspace')).reply(200, put_config_airspace);
	// 接口：post 部署设置 // post_deploy_setting 
	mock.onPost(new RegExp('.*/v1/interpulse/deploy/setting')).reply(200, post_deploy_setting);
	// 接口：post 上传到知识库 // post_interpulse_knowledge 
	mock.onPost(new RegExp('.*/v1/interpulse/knowledge')).reply(200, post_interpulse_knowledge);
	// 接口：get 获取主框架添加到知识库Url // get_knowledge_url 
	mock.onGet(new RegExp('.*/v1/interpulse/knowledge/url')).reply(200, get_knowledge_url);
	// 接口：post 设置语言 // post_interpulse_language 
	mock.onPost(new RegExp('.*/v1/interpulse/language')).reply(200, post_interpulse_language);
	// 接口：get 是否存在pdw文件 // get_pdw_exist 
	mock.onGet(new RegExp('.*/v1/interpulse/pdw/exist')).reply(200, get_pdw_exist);
	// 接口：delete deletePdwFile // delete_pdw_file 
	mock.onDelete(new RegExp('.*/v1/interpulse/pdw/file')).reply(200, delete_pdw_file);
	// 接口：delete deleteAllPdwFile // delete_file_all 
	mock.onDelete(new RegExp('.*/v1/interpulse/pdw/file/all')).reply(200, delete_file_all);
	// 接口：get 获取pdw文件结果 // get_pdw_files 
	mock.onGet(new RegExp('.*/v1/interpulse/pdw/files')).reply(200, get_pdw_files);
	// 接口：get 从pdw文件中读取数据 // get_pdw_result 
	mock.onGet(new RegExp('.*/v1/interpulse/pdw/result')).reply(200, get_pdw_result);
	// 接口：post 从pdw文件中读取数据 // post_pdw_result 
	mock.onPost(new RegExp('.*/v1/interpulse/pdw/result')).reply(200, post_pdw_result);
	// 接口：get 从pdw文件中读取子PDW数据 // get_result_child 
	mock.onGet(new RegExp('.*/v1/interpulse/pdw/result/child')).reply(200, get_result_child);
	// 接口：get 文件另存为 // get_pdw_save 
	mock.onGet(new RegExp('.*/v1/interpulse/pdw/save')).reply(200, get_pdw_save);
	// 接口：post saveData // post_save_data 
	mock.onPost(new RegExp('.*/v1/interpulse/pdw/save/data')).reply(200, post_save_data);
	// 接口：post PDW 数据传输 // post_pdw_transfer 
	mock.onPost(new RegExp('.*/v1/interpulse/pdw/transfer')).reply(200, post_pdw_transfer);
	// 接口：post 设置PDW过滤 // post_interpulse_pdw_filter 
	mock.onPost(new RegExp('.*/v1/interpulse/pdw_filter')).reply(200, post_interpulse_pdw_filter);
	// 接口：get 获取当前站的pdw过滤配置 // get_pdw_config 
	mock.onGet(new RegExp('.*/v1/interpulse/pulse/pdw/config')).reply(200, get_pdw_config);
	// 接口：get 获取文件的pdw结果 // get_pdw_sav 
	mock.onGet(new RegExp('.*/v1/interpulse/pulse/pdw/sav')).reply(200, get_pdw_sav);
	// 接口：get 获取文件的pdw结果（socket异步） // get_sav_async 
	mock.onGet(new RegExp('.*/v1/interpulse/pulse/pdw/sav/async')).reply(200, get_sav_async);
	// 接口：put 停止解析sav文件 // put_sav_stop 
	mock.onPut(new RegExp('.*/v1/interpulse/pulse/pdw/sav/stop')).reply(200, put_sav_stop);
	// 接口：get 获取当前雷达工作模式 // get_radar_mode 
	mock.onGet(new RegExp('.*/v1/interpulse/radar/mode')).reply(200, get_radar_mode);
	// 接口：post 设置当前雷达工作模式 // post_radar_mode 
	mock.onPost(new RegExp('.*/v1/interpulse/radar/mode')).reply(200, post_radar_mode);
	// 接口：get 获得子PDW的设置 // get_interpulse_start_read_child_pdw 
	mock.onGet(new RegExp('.*/v1/interpulse/start_read_child_pdw')).reply(200, get_interpulse_start_read_child_pdw);
	// 接口：post 开始读取子PDW // post_interpulse_start_read_child_pdw 
	mock.onPost(new RegExp('.*/v1/interpulse/start_read_child_pdw')).reply(200, post_interpulse_start_read_child_pdw);
	// 接口：get 获得实时分析PDW的设置 // get_interpulse_start_read_pdw 
	mock.onGet(new RegExp('.*/v1/interpulse/start_read_pdw')).reply(200, get_interpulse_start_read_pdw);
	// 接口：post 开始读取PDW // post_interpulse_start_read_pdw 
	mock.onPost(new RegExp('.*/v1/interpulse/start_read_pdw')).reply(200, post_interpulse_start_read_pdw);
	// 接口：post 开始读取PDW（内部测试） // post_start_read_pdw_test 
	mock.onPost(new RegExp('.*/v1/interpulse/start_read_pdw/test')).reply(200, post_start_read_pdw_test);
	// 接口：delete 停止读取子PDW // delete_interpulse_stop_read_child_pdw 
	mock.onDelete(new RegExp('.*/v1/interpulse/stop_read_child_pdw')).reply(200, delete_interpulse_stop_read_child_pdw);
	// 接口：delete 停止读取PDW // delete_interpulse_stop_read_pdw 
	mock.onDelete(new RegExp('.*/v1/interpulse/stop_read_pdw')).reply(200, delete_interpulse_stop_read_pdw);
	// 接口：post 工作模式设置 // post_work_mode 
	mock.onPost(new RegExp('.*/v1/interpulse/work/mode')).reply(200, post_work_mode);
	// 接口：get 获取pdw文件结果 // get_pdw_files 
	mock.onGet(new RegExp('.*/v2/interpulse/pdw/files')).reply(200, get_pdw_files);
}