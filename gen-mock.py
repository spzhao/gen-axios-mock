#!/usr/bin/python3
import json
import os
from urllib import request

# swagger API接口数据生成 axios-mock 的代码

BLACK_LIST = ['InputStreamResource']

methodDict = dict()
methodDict['get'] = 'onGet'
methodDict['put'] = 'onPut'
methodDict['post'] = 'onPost'
methodDict['delete'] = 'onDelete'

defaultValue = dict()
defaultValue['number'] = 0
defaultValue['integer'] = 0
defaultValue['boolean'] = False
defaultValue['string'] = 'test-string'
defaultValue['object'] = None


methodTemplate = '\t// 接口：{title} // {response_name} \n\tmock.{methodName}(new RegExp(\'.*{url}\')).reply(200, {data});'
importTemplate = '''import MockAdapter from 'axios-mock-adapter'
import {{\n\t{names}}} from \'{jsFileName}\''''
exportFileTemplate = '''
// {desc}
export const {name} = {data}
'''
functionTemplate = '''
const initMock = (axiosInstance) => {{
    const mock = new MockAdapter(axioInstance);
    {mocklines}
}}'''

jsFileName = './mock-data.js'
mockFileName = './axiosMock.js'
mockNameList = list()
mockDataList = list()

class MockData:
    name = ''
    desc = ''
    methodLine = ''
    data = '{}'
    def __init__(self, name, data, desc, methodLine):
        self.name = name
        self.data = data
        self.desc = desc
        self.methodLine = methodLine

def getApiJson(url):
    # print(dir(request))
    print('URL=',url)
    data = request.urlopen(url).read()
    # print('RESULT=',data)
    return data

def getMockMethod(method):
    '''
    获取mock对应的方法，get->onGet
    '''
    if (method in methodDict):
       return methodDict[method]
    return None

def getResponseDataJson(defineDict, key):
    '''
    解析response为json或者dict类型数据
    '''
    # print(key)
    k = key.split('/')[-1]
    # 黑名单
    if (k in BLACK_LIST):
        return None
    d = defineDict[k]
    r = dict()
    if ('properties' in d):
        p = d['properties']
        for attr in p:
            _a = p[attr]
            hasMore = '$ref' in _a or 'items' in _a or 'type' not in _a;
            if (not hasMore):
                _t = p[attr]['type']
                if (_t == 'string') :
                    r[attr] = attr
                else:
                    r[attr] = defaultValue[_t]
            else:
                if ('$ref' in p[attr]):
                    r[attr] = getResponseDataJson(defineDict, p[attr]['$ref'])
                if ('items' in p[attr]):
                    r[attr] = list()
                    r[attr].append(getResponseDataJson(defineDict, p[attr]['items']['$ref']))
    return r

def write2ImportFile(fileName, fromName, dataList):
    '''
    写入到import文件
    @param dataList List<MockData>
    '''
    if (os.path.exists(fileName)):
        os.remove(fileName)
    f = open(fileName,'a+', encoding='utf8')
    names = list()
    for mockData in dataList:
        names.append(mockData.name)
    value = importTemplate.format(names=',\n\t'.join(names), jsFileName=fromName)
    f.write(value)
    lines = list()
    for l in dataList:
        lines.append(l.methodLine)
    f.write(functionTemplate.format(mocklines='\n'.join(lines)))
    f.close()
    return None
    
def write2ExportFile(fileName, dataList):
    '''
    写入到export文件（mock数据部分）
    @param dataList List<MockData>
    '''
    if (os.path.exists(fileName)):
        os.remove(fileName)
    f = open(fileName,'a+', encoding='utf8')
    for mockData in dataList:
        value = exportFileTemplate.format(name=mockData.name, data=json.dumps(mockData.data), desc=mockData.desc)
        f.write(value)
    f.close()
    return None

# print(importTemplate.format('hello'))

if __name__ == "__main__":
    _data = getApiJson("http://localhost:8306/v2/api-docs")
    data = json.loads(_data)
    data = dict(data)
    for p in data['paths']:
        for method in data['paths'][p]:
            c = data['paths'][p][method]
            _title = method + ' ' + str(c['summary'])
            resp = c['responses']['200']
            responseData = dict(code=0)
            _last_name = '_'.join(p.split('/')[-2:])
            response_name = '{method}_{last_name}'.format(method=method,last_name=_last_name)
            if ('schema' in resp):
                responseData = c['responses']['200']['schema']['$ref']
            if ('#' in responseData):
                responseData = getResponseDataJson(data['definitions'], responseData)
            mockMethodLine = methodTemplate.format(url=p, data=response_name, title=_title, methodName=getMockMethod(method), response_name = response_name)
            print(mockMethodLine)
            mockDataList.append(MockData(response_name, responseData, _title, mockMethodLine))
            mockNameList.append(response_name)
            # print(getTemplte.format(url=p, data='{}', title=method))
    write2ExportFile(jsFileName, mockDataList)
    write2ImportFile(mockFileName, jsFileName, mockDataList)