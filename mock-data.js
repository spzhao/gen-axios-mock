
// get deploy
export const get_test_deploy = {"seat": "seat", "softName": "softName", "stationId": 0, "taskId": "taskId", "userName": "userName"}

// put intraResult
export const put_test_intra_result = {"code": 0}

// get language
export const get_test_language = {"language": 0}

// get workMode
export const get_test_work_mode = {"workMode": 0}

// get 获得事后脉间分析PDW的设置
export const get_interpulse_after_pdw_setting = {"amplitudeEnd": 0, "amplitudeStart": 0, "collectCount": 0, "collectTime": 0, "filterAmplitude": false, "filterFrequency": false, "filterInterpulse": false, "filterOrientation": false, "filterPulseWidth": false, "frequencyEnd": 0, "frequencyStart": 0, "interpulseEnd": 0, "interpulseStart": 0, "orientationEnd": 0, "orientationStart": 0, "pulseWidthEnd": 0, "pulseWidthStart": 0, "receiverList": "receiverList", "receiverNumber": 0, "repeatCycle": "repeatCycle", "stationNumber": 0, "type": 0}

// get getAirSpaceConfig
export const get_config_airspace = {"code": 0, "data": null, "msg": "msg"}

// put setSaveAirSpaceConfig
export const put_config_airspace = {"code": 0, "data": null, "msg": "msg"}

// post 部署设置
export const post_deploy_setting = {"code": 0, "data": null, "msg": "msg"}

// post 上传到知识库
export const post_interpulse_knowledge = {"code": 0, "data": null, "msg": "msg"}

// get 获取主框架添加到知识库Url
export const get_knowledge_url = {"code": 0, "data": null, "msg": "msg"}

// post 设置语言
export const post_interpulse_language = {"code": 0}

// get 是否存在pdw文件
export const get_pdw_exist = {"code": 0, "data": null, "msg": "msg"}

// delete deletePdwFile
export const delete_pdw_file = {"code": 0, "data": null, "msg": "msg"}

// delete deleteAllPdwFile
export const delete_file_all = {"code": 0, "data": null, "msg": "msg"}

// get 获取pdw文件结果
export const get_pdw_files = {"code": 0, "data": [{"id": "id", "pdwData": "pdwData", "setAz": "setAz", "setAzMax": 0, "setAzMin": 0, "setFreq": "setFreq", "setFreqMax": 0, "setFreqMin": 0, "setNum": 0, "setPa": "setPa", "setPaMax": 0, "setPaMin": 0, "setPri": "setPri", "setPriMax": 0, "setPriMin": 0, "setPw": "setPw", "setPwMax": 0, "setPwMin": 0, "setReceiver": 0, "setSelect": 0, "setTime": 0, "taskId": "taskId", "time": "time", "userName": "userName"}], "msg": "msg"}

// get 从pdw文件中读取数据
export const get_pdw_result = {"code": 0, "data": {"isStop": false, "pdwData": [{"arriveTime": "arriveTime", "az": 0, "freqMax": 0, "freqMin": 0, "interceptTime": "interceptTime", "pa": 0, "pri": 0, "pw": 0, "time": "time", "toa": 0, "type": 0}]}, "msg": "msg"}

// post 从pdw文件中读取数据
export const post_pdw_result = {"code": 0, "data": {"isStop": false, "pdwData": [{"arriveTime": "arriveTime", "az": 0, "freqMax": 0, "freqMin": 0, "interceptTime": "interceptTime", "pa": 0, "pri": 0, "pw": 0, "time": "time", "toa": 0, "type": 0}]}, "msg": "msg"}

// get 从pdw文件中读取子PDW数据
export const get_result_child = {"code": 0, "data": {"isStop": false, "pdwData": [{"arriveTime": "arriveTime", "az": 0, "freqMax": 0, "freqMin": 0, "interceptTime": "interceptTime", "pa": 0, "pri": 0, "pw": 0, "time": "time", "toa": 0, "type": 0}]}, "msg": "msg"}

// get 文件另存为
export const get_pdw_save = null

// post saveData
export const post_save_data = {"code": 0, "data": null, "msg": "msg"}

// post PDW 数据传输
export const post_pdw_transfer = {"code": 0, "data": null, "msg": "msg"}

// post 设置PDW过滤
export const post_interpulse_pdw_filter = {"filterFrequency": false, "filterPulseWidth": false, "frequencyEnd": 0, "frequencyStart": 0, "pulseWidthEnd": 0, "pulseWidthStart": 0}

// get 获取当前站的pdw过滤配置
export const get_pdw_config = {"code": 0, "data": {"amMax": 0, "amMin": 0, "azMax": 0, "azMin": 0, "collectNumbersMax": 0, "collectNumbersMin": 0, "collectTimeMax": 0, "collectTimeMin": 0, "freqMax": 0, "freqMin": 0, "frequencyRanges": [{"max": 0, "min": 0}], "pulseWidthMax": 0, "pulseWidthMin": 0, "repeatMax": 0, "repeatMin": 0}, "msg": "msg"}

// get 获取文件的pdw结果
export const get_pdw_sav = {"code": 0, "data": [{"arriveTime": "arriveTime", "az": 0, "freqMax": 0, "freqMin": 0, "interceptTime": "interceptTime", "pa": 0, "pri": 0, "pw": 0, "time": "time", "toa": 0, "type": 0}], "msg": "msg"}

// get 获取文件的pdw结果（socket异步）
export const get_sav_async = {"code": 0, "data": null, "msg": "msg"}

// put 停止解析sav文件
export const put_sav_stop = {"code": 0, "data": null, "msg": "msg"}

// get 获取当前雷达工作模式
export const get_radar_mode = {"code": 0, "data": null, "msg": "msg"}

// post 设置当前雷达工作模式
export const post_radar_mode = {"code": 0, "data": null, "msg": "msg"}

// get 获得子PDW的设置
export const get_interpulse_start_read_child_pdw = {"amplitudeEnd": 0, "amplitudeStart": 0, "collectCount": 0, "collectTime": 0, "filterAmplitude": false, "filterFrequency": false, "filterInterpulse": false, "filterOrientation": false, "filterPulseWidth": false, "frequencyEnd": 0, "frequencyStart": 0, "interpulseEnd": 0, "interpulseStart": 0, "orientationEnd": 0, "orientationStart": 0, "pulseWidthEnd": 0, "pulseWidthStart": 0, "receiverList": "receiverList", "receiverNumber": 0, "repeatCycle": "repeatCycle", "stationNumber": 0, "type": 0}

// post 开始读取子PDW
export const post_interpulse_start_read_child_pdw = {"fileName": "fileName", "start": false}

// get 获得实时分析PDW的设置
export const get_interpulse_start_read_pdw = {"amplitudeEnd": 0, "amplitudeStart": 0, "collectCount": 0, "collectTime": 0, "filterAmplitude": false, "filterFrequency": false, "filterInterpulse": false, "filterOrientation": false, "filterPulseWidth": false, "frequencyEnd": 0, "frequencyStart": 0, "interpulseEnd": 0, "interpulseStart": 0, "orientationEnd": 0, "orientationStart": 0, "pulseWidthEnd": 0, "pulseWidthStart": 0, "receiverList": "receiverList", "receiverNumber": 0, "repeatCycle": "repeatCycle", "stationNumber": 0, "type": 0}

// post 开始读取PDW
export const post_interpulse_start_read_pdw = {"cacheFileName": "cacheFileName", "start": false}

// post 开始读取PDW（内部测试）
export const post_start_read_pdw_test = {"cacheFileName": "cacheFileName", "start": false}

// delete 停止读取子PDW
export const delete_interpulse_stop_read_child_pdw = {"fileName": "fileName", "start": false}

// delete 停止读取PDW
export const delete_interpulse_stop_read_pdw = {"cacheFileName": "cacheFileName", "start": false}

// post 工作模式设置
export const post_work_mode = {"code": 0}

// get 获取pdw文件结果
export const get_pdw_files = {"code": 0, "data": {"list": [{"id": "id", "pdwData": "pdwData", "setAz": "setAz", "setAzMax": 0, "setAzMin": 0, "setFreq": "setFreq", "setFreqMax": 0, "setFreqMin": 0, "setNum": 0, "setPa": "setPa", "setPaMax": 0, "setPaMin": 0, "setPri": "setPri", "setPriMax": 0, "setPriMin": 0, "setPw": "setPw", "setPwMax": 0, "setPwMin": 0, "setReceiver": 0, "setSelect": 0, "setTime": 0, "taskId": "taskId", "time": "time", "userName": "userName"}], "pagination": {"page": 0, "total": 0}}, "msg": "msg"}
